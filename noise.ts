
/**
 * Common functions found in various noise shaders.
 */
export default {


    commonNoiseFunctions:`
        fn mod289(x:vec4<f32>)->vec4<f32>{
            return x - floor(x * (1.0 / 289.0)) * 289.0;
        }
        fn mod289(x:f32)->f32{
            return x - floor(x * (1.0 / 289.0)) * 289.0;
        }
        fn permute(x:vec4<f32>)->vec4<f32>{
            return mod289(((x*34.0)+1.0)*x);
        }
        
        fn permute(x:f32)->f32{
            return mod289(((x*34.0)+1.0)*x);
        }
        
        fn taylorInvSqrt(r:vec4<f32>)->vec4<f32>{
           return 1.79284291400159 - 0.85373472095314 * r;
        }
        
        fn taylorInvSqrt(r:f32)->f32{
           return 1.79284291400159 - 0.85373472095314 * r;
        }
    `,

    grad4:
    `
        fn grad4(j:f32, ip:vec4<f32>)->vec4<f32>{
            var ones:vec4<f32> = vec4<f32>(1.0, 1.0, 1.0, -1.0);
            var p:vec4<f32>;
            var s:vec4<f32>;

            p.xyz = floor( fract (vec3<f32>(j,j,j) * ip.xyz) * 7.0) * ip.z - 1.0;
            p.w = 1.5 - dot(abs(p.xyz), ones.xyz);
            s = vec4<f32>(lessThan(p, vec4<f32>(0.0,0.0.0.0,0.0)));
            p.xyz = p.xyz + (s.xyz*2.0 - 1.0) * s.www; 

            return p;
        }
    
    `
}