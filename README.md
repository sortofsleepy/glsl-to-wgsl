# Useful GLSL to WGSL shaders

A set of useful shaders that have been converted from GLSL to WGSL for use in WebGPU applications.


Description 
===
* All of the shaders in here are potentially useful sets of shaders for use with WebGPU and have been converted from GLSL. 
* These are in a `.ts` format for the time being untill the spec is more finalized and IDE support is more widely available. 
* vertex and fragment shaders are combined and should hopefully be easy to extract into other contexts(like when using Rust) as needed; however note that you may still need to tweak things depending on your setup.
